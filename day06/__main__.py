from utils.lists import import_lists


def part1():
    data = import_lists(__package__, use_example=False)
    data = data[0]

    marker = ''
    for idx, chr in enumerate(data):
        marker += chr
        if len(set(marker[-4:])) == 4:
            return idx+1


def part2():
    data = import_lists(__package__, use_example=False)
    data = data[0]

    marker = ''
    for idx, chr in enumerate(data):
        marker += chr
        if len(set(marker[-14:])) == 14:
            return idx+1


if __name__ == '__main__':
    print(part1())
    print(part2())