from utils.lists import import_lists
import re


def part1():
    data = import_lists(__package__, use_example=False)

    # Formatter le fichier
    stack, instructions = data_to_objects(data)

    # Bouger les boites
    for instruction in instructions:
        for _ in range(instruction['count']):
            box_to_move = stack[instruction['from']].pop(0)
            stack_to = stack[instruction['to']]
            stack_to.insert(0, box_to_move)

    # Obtenir la réponse
    reponse = ''
    for column in stack:
        reponse += column[0]

    return reponse


def part2():
    data = import_lists(__package__, use_example=False)

    # Formatter le fichier
    stack, instructions = data_to_objects(data)

    # Bouger les boites
    for instruction in instructions:
        boxes_to_move = []
        for _ in range(instruction['count']):
            boxes_to_move.append(stack[instruction['from']].pop(0))
        stack[instruction['to']] = boxes_to_move + stack[instruction['to']]

    # Obtenir la réponse
    reponse = ''
    for column in stack:
        reponse += column[0]

    return reponse


def data_to_objects(data):
    list_stacks_raw = []
    list_stacks = []
    list_instructions_raw = []
    list_instructions = []
    num_cols = 0

    # Séparer le stack des instructions
    for idx, line in enumerate(data):
        # Séparation entre le stack et les instructions
        if line == '':
            list_instructions_raw = data[idx+1:]
            break
        list_stacks_raw.append(format_stack_line(line))

    # Trouver le nombre de colonnes
    num_cols = len(list_stacks_raw[-1])
    list_stacks_raw.pop()
        
    # Transformer le stack en liste
    for _ in range(num_cols):
        list_stacks.append([])

    for line in list_stacks_raw:
        for idx, char in enumerate(line):
            if char == '|':
                continue
            list_stacks[idx].append(char)

    # Transformer les instructions
    for line in list_instructions_raw:
        instruction = line.split(' ')
        list_instructions.append({
            'count': int(instruction[1]),
            'from': int(instruction[3]) - 1,
            'to': int(instruction[5]) - 1
        })    

    return list_stacks, list_instructions


def format_stack_line(line):
    regex = r'[\s]{4}'
    line = re.sub(regex, '|', line, 0, re.MULTILINE)
    # line = line.replace('    ', '|')
    line = line.replace(' ', '').replace('[', '').replace(']', '')
    return line


if __name__ == '__main__':
    print(part1())
    print(part2())