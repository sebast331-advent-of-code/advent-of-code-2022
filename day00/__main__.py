from utils.lists import import_lists


def part1():
    data = import_lists(__package__, use_example=True)


def part2():
    data = import_lists(__package__, use_example=True)


if __name__ == '__main__':
    print(part1())
    print(part2())