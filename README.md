# Advent of Code 2022
[![pipeline status](https://gitlab.com/sebast331-advent-of-code/advent-of-code-2022/badges/main/pipeline.svg)](https://gitlab.com/sebast331-advent-of-code/advent-of-code-2022/-/commits/main)
[![coverage report](https://gitlab.com/sebast331-advent-of-code/advent-of-code-2022/badges/main/coverage.svg)](https://gitlab.com/sebast331-advent-of-code/advent-of-code-2022/-/commits/cicd)



## Mes réponses à ce jour
[Fichier texte des réponses](https://gitlab.com/api/v4/projects/41584916/jobs/artifacts/main/raw/reponses.txt?job=export-tableau)
![Dernier fichier de réponse](https://gitlab.com/api/v4/projects/41584916/jobs/artifacts/main/raw/reponses.png?job=export-image)
## Installation
```
git clone https://gitlab.com/sebast331-advent-of-code/advent-of-code-2022.git
cd advent-of-code-2022
python -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
```
![Exécution du code](images/all_days.png)

## Exécution du code
### Exécuter toutes les journées
Toutes les journées sont affichées grâce à un chagement dynamique des modules :
```
python all.py
```

### Exécuter seulement une journée
Une seule journée peut être exécuté en tant que module :
```
python -m day01
```

## Ajout d'une nouvelle journée
Chaque nouvelle journée doit :
1. être contenu dans un dossier à son nom suivant le format `dayXX`, où `XX` est un nombre entre 01 et 25, incluant un zéro
2. avoir le code principal dans le fichier `__main__.py`

Le fichier `__main__.py` doit posséder minimalement le code suivant :
```python
def part1():
    pass

def part2():
    pass

if __name__ == '__main__':
    print(part1())
    print(part2())
```
Celui-ci sera alors inclu automatiquement pour le script complet.

## Intégration avec Visual Studio Code
Une intégration peut être faite avec Visual Studio Code afin de faciliter l'exécution du code. Voir le fichier `.vscode/launch.json`.

![Exécution du code](images/integration_vscode.png)