import utils.lists
import day01.__main__ as day01
import day02.__main__ as day02
import day03.__main__ as day03
import day04.__main__ as day04

def test_utils():
    # Petite liste example
    with open('./day01/small.txt') as f:
        small = f.read().splitlines()
    assert utils.lists.import_lists('day01', use_example=True) == small
    # Grande liste example
    with open('./day01/large.txt') as f:
        large = f.read().splitlines()
    assert utils.lists.import_lists('day01', use_example=False) == large

def test_day01():
    # Retourne un int
    assert type(day01.part1()) == int
    assert type(day01.part2()) == int

def test_day02():
    # Retourne un int
    assert type(day02.part1()) == int
    assert type(day02.part2()) == int
    # Draw
    assert day02.get_outcome('A', 'X') == 'D'
    assert day02.get_outcome('B', 'Y') == 'D'
    assert day02.get_outcome('C', 'Z') == 'D'
    # Win
    assert day02.get_outcome('A', 'Y') == 'W'
    assert day02.get_outcome('B', 'Z') == 'W'
    assert day02.get_outcome('C', 'X') == 'W'
    # Lose
    assert day02.get_outcome('A', 'Z') == 'L'
    assert day02.get_outcome('B', 'X') == 'L'
    assert day02.get_outcome('C', 'Y') == 'L'

    # Round Score
    assert day02.get_round_score('A', 'Y') == 8
    assert day02.get_round_score('B', 'X') == 1
    assert day02.get_round_score('C', 'Z') == 6

    # Wanted Score
    assert day02.find_my_choice('A', 'X') == 'Z'
    assert day02.find_my_choice('A', 'Y') == 'X'
    assert day02.find_my_choice('A', 'Z') == 'Y'
    assert day02.find_my_choice('B', 'X') == 'X'
    assert day02.find_my_choice('B', 'Y') == 'Y'
    assert day02.find_my_choice('B', 'Z') == 'Z'
    assert day02.find_my_choice('C', 'X') == 'Y'
    assert day02.find_my_choice('C', 'Y') == 'Z'
    assert day02.find_my_choice('C', 'Z') == 'X'

def test_day03():
    # Retourne un int
    assert type(day03.part1()) == int
    assert type(day03.part2()) == int

def test_day04():
    # Retourne un int
    assert type(day04.part1()) == int
    assert type(day04.part2()) == int
    # 
    assert day04.section_to_list('2-4') == [2, 3, 4]
    assert day04.section_to_list('1-9') == list(range(1, 10))
