from utils.lists import import_lists


def part1():
    data = import_lists(__package__)
    combined_calories = ','.join(data).split(',,')
    sum_of_calories = [ sum(map(int, x.split(','))) for x in combined_calories ]
    return max(sum_of_calories)


def part2():
    data = import_lists(__package__)
    combined_calories = ','.join(data).split(',,')
    sum_of_calories = sorted([ sum(map(int, x.split(','))) for x in combined_calories ], reverse=True)
    return sum(sum_of_calories[0:3])


if __name__ == '__main__':
    print(part1())
    print(part2())