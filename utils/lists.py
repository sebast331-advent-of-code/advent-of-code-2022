def import_lists(package, use_example=False):
    if use_example:
        with open(f'./{package}/small.txt') as f:
            data = f.read().splitlines()
    else:
        with open(f'./{package}/large.txt') as f:
            data = f.read().splitlines()
    return data