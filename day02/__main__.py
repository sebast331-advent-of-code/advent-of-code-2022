from utils.lists import import_lists


def part1():
    data = import_lists(__package__, use_example=False)
    score = 0
    for round in data:
        opponent_choice, my_choice = round.split(' ')
        score += get_round_score(opponent_choice, my_choice)
    
    return score


def part2():
    data = import_lists(__package__, use_example=False)
    score = 0
    for round in data:
        opponent_choice, end_result = round.split(' ')
        my_choice = find_my_choice(opponent_choice, end_result)
        score += get_round_score(opponent_choice, my_choice)

    return score



def get_round_score(opponent_choice, my_choice):
    points = {
        'L': 0,
        'D': 3,
        'W': 6,
        'X': 1,
        'Y': 2,
        'Z': 3
    }
    score = points[get_outcome(opponent_choice, my_choice)] + points[my_choice]
    return score


def get_outcome(opponent_choice, my_choice):
    if opponent_choice == 'A' and my_choice == 'Y': return 'W'
    if opponent_choice == 'B' and my_choice == 'Z': return 'W'
    if opponent_choice == 'C' and my_choice == 'X': return 'W'
    if opponent_choice == 'A' and my_choice == 'X': return 'D'
    if opponent_choice == 'B' and my_choice == 'Y': return 'D'
    if opponent_choice == 'C' and my_choice == 'Z': return 'D'
    
    return 'L'


def find_my_choice(opponent_choice, wanted):
    choices = {
        'A': {
            'X': 'Z',
            'Y': 'X',
            'Z': 'Y'
        },
        'B': {
            'X': 'X',
            'Y': 'Y',
            'Z': 'Z',
        },
        'C': {
            'X': 'Y',
            'Y': 'Z',
            'Z': 'X'
        }
    }
    return choices[opponent_choice][wanted]


if __name__ == '__main__':
    print(part1())
    print(part2())