from utils.lists import import_lists
import pydash


def part1():
    data = import_lists(__package__, use_example=False)
    count = 0
    for section in data:
        first, second = section.split(',')
        first_list = section_to_list(first)
        second_list = section_to_list(second)
        
        if pydash.intersection(first_list, second_list) == first_list or pydash.intersection(first_list, second_list) == second_list:
            count += 1

    return count


def part2():
    data = import_lists(__package__, use_example=False)
    count = 0
    for section in data:
        first, second = section.split(',')
        first_list = section_to_list(first)
        second_list = section_to_list(second)
        
        if len(pydash.intersection(first_list, second_list)):
            count += 1

    return count


def section_to_list(section):
    first, last = section.split('-')
    sections = list(range(int(first), int(last)+1))
    return sections


if __name__ == '__main__':
    print(part1())
    print(part2())