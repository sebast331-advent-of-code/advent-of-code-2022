from importlib import import_module

def print_entete():
    # Entête
    print()
    print('     **          **                             **                ****     ******                **        ')
    print('    ****        /**                            /**               /**/     **////**              /**        ')
    print('   **//**       /** **    **  *****  *******  ******    ******  ******   **    //   ******      /**  ***** ')
    print('  **  //**   ******/**   /** **///**//**///**///**/    **////**///**/   /**        **////**  ****** **///**')
    print(' ********** **///**//** /** /******* /**  /**  /**    /**   /**  /**    /**       /**   /** **///**/*******')
    print('/**//////**/**  /** //****  /**////  /**  /**  /**    /**   /**  /**    //**    **/**   /**/**  /**/**//// ')
    print('/**     /**//******  //**   //****** ***  /**  //**   //******   /**     //****** //****** //******//******')
    print('//      //  //////    //     ////// ///   //    //     //////    //       //////   //////   //////  ////// ')
    print()


def print_entete_tableau():
    # Entête du tableau
    print('-' * 64)
    print(f'| Etoiles |  {"Jour":<8}|  {"Star 1":<18}|  {"Star 2":<18}|')
    print('-' * 64)


def print_ligne_tableau(jour, star1, star2):
    star1 = '' if star1 is None else star1
    star2 = '' if star2 is None else star2
    stars = sum([1 for i in [star1, star2] if i != ''])
    print(f'| {stars * "*":^7} |  {jour:<8}|  {star1:<18}|  {star2:<18}|')


def print_pied_tableau():
    print('-' * 64)


def main():
    print_entete()
    print_entete_tableau()
    for i in range(1, 26):
        module_name = f'day{i:02}.__main__'

        try:
            module = import_module(module_name)
            print_ligne_tableau(f'Day {i:02}', module.part1(), module.part2())
        except ModuleNotFoundError:
            print_ligne_tableau(f'Day {i:02}', '', '')
    print_pied_tableau()


if __name__ == '__main__':
    main()