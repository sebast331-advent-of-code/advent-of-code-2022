from utils.lists import import_lists
import string
import pydash


def part1():
    data = import_lists(__package__, use_example=False)
    priorities = string.ascii_lowercase + string.ascii_uppercase
    rucksack_priorities = []
    for rucksack in data:
        first, second = pydash.chunk(rucksack, int(len(rucksack)/2))
        intersect = pydash.intersection(list(first), list(second))[0]
        rucksack_priorities.append(priorities.index(intersect) + 1)

    return sum(rucksack_priorities)


def part2():
    data = import_lists(__package__, use_example=False)
    priorities = string.ascii_lowercase + string.ascii_uppercase
    rucksack_priorities = []
    chunks = pydash.chunk(data, 3)
    for chunk in chunks:
        intersect = pydash.intersection(list(chunk[0]), list(chunk[1]), list(chunk[2]))[0]
        rucksack_priorities.append(priorities.index(intersect) + 1)

    return sum(rucksack_priorities)



if __name__ == '__main__':
    print(part1())
    print(part2())